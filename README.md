My dotfiles
===========

This project was inspired by the following [Using GNU Stow to manage your dotfiles](http://brandon.invergo.net/news/2012-05-26-using-gnu-stow-to-manage-your-dotfiles.html)

Requirements

- bash
- git
- stow

Example:

```shell
    $ cd ~
    $ git clone https://gitlab.com/oak155online/dotfiles.git
    $ stow xdg-user-dirs bash bash-completion git vim tmux
    $ # stow PACKAGE
```

