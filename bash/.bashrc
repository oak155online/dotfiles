# ~/.bashrc:

# If not running interactively, don't do anything
case $- in
    *i*) ;;
      *) return
		  ;;
esac

# enable color support of ls and also add handy aliases
if [ -x /usr/bin/dircolors ] ; then
    test -r ~/.dircolors && eval "$(dircolors -b ~/.dircolors)" || eval "$(dircolors -b)"
    alias ls='ls --color=auto'
    alias dir='dir --color=autoi'
    alias vdir='vdir --color=auto'

    alias grep='grep --color=auto'
    alias fgrep='fgrep --color=auto'
    alias egrep='egrep --color=auto'
fi

# colored GCC warnings and errors
export GCC_COLORS='error=01;31:warning=01;35:note=01;36:caret=01;32:locus=01:quote=01'

alias cp='cp -i'
# alias grep='grep --color=auto'
alias ivy='java -jar ${HOME}/.ant/lib/ivy.jar -settings ${XDG_CONFIG_HOME:-$HOME/.config}/ivy2/ivysettings.xml'
alias la='ls -a'
alias links2='links2 -bookmarks-file $(xdg-user-dir DOCUMENTS)/links2/bookmarks.html -download-dir $(xdg-user-dir DOWNLOAD)'
alias ll='ls -l'
alias man='env LC_CTYPE=POSIX man'
alias mvn='mvn -s ${XDG_CONFIG_HOME:-$HOME/.config}/mvn/settings.xml'
alias pipenv='PIP_USER="false" pipenv'
alias python='python -tt'
alias python2='python2 -tt'
alias rm='rm -i'
alias ssh-add='ssh-add -E md5'
alias ssh-agent='ssh-agent -E md5'
alias ssh-keygen='ssh-keygen -E md5'
alias start_ssh_agent='eval `ssh-agent -s`'
alias tmux_2ph='tmux new-session \; split-window -dh'
alias tmux_2pv='tmux new-session \; split-window -dv'
alias tmux_4p='tmux new-session \; split-window -h \; split-window -v \; select-pane -t .1 \; split-window -dv'
alias watch_newsboat_reload='watch -n 10 systemctl --user status newsboat-reload-all.service'

# if [ -n "${BASH_VERSION}" ] ; then
# 	eval "$(_MOLECULE_COMPLETE=source molecule)"
# fi

if [ -n "${BASH_VERSION}" ] ; then
	complete -C "${HOME}/.local/bin/terraform" terraform
fi

