# ~/.bash_profile:
TZ=Europe/London ; export TZ

GPG_TTY=$(tty) ; export GPG_TTY

XDG_DATA_HOME="${HOME}/.local/share" ; export XDG_DATA_HOME
XDG_CONFIG_HOME="${HOME}/.config" ; export XDG_CONFIG_HOME
XDG_DATA_DIRS='/usr/local/share:/usr/share' ; export XDG_DATA_DIRS
XDG_CONFIG_DIRS='/etc/xdg' ; export XDG_CONFIG_DIRS
XDG_CACHE_HOME="${HOME}/.cache" ; export XDG_CACHE_HOME

HISTFILE= ; export HISTFILE
if [ -n "${BASH_VERSION}" ] ; then
	shopt -s histappend
fi
HISTCONTROL=ignoreboth
HISTSIZE=1000
HISTFILESIZE=2000

LESSHISTFILE=- ; export LESSHISTFILE

# used in screen and tmux
LOCKPG=vlock; export LOCKPG

RSH=/usr/bin/ssh ; export RSH

# see the following: <https://www.freedesktop.org/software/systemd/man/file-hierarchy.html#~/.local/bin/>
if [ -d "${HOME}/.local/bin" ] ; then
    PATH="${HOME}/.local/bin:$PATH"
fi

# set PATH so it includes user's private bin if it exists
if [ -d "${HOME}/bin" ] ; then
    PATH="${HOME}/bin:$PATH"
fi

if [ -n "${BASH_VERSION}" ] ; then
	# Bash prompt customization for Git
	#   GIT_PS1_SHOWDIRTYSTATE     + for staged, * if unstaged.
	#   GIT_PS1_SHOWSTASHSTATE     $ if something is stashed.
	#   GIT_PS1_SHOWUNTRACKEDFILES % if there are untracked files.
	#
	GIT_PS1_SHOWCOLORHINTS=1 ; export GIT_PS1_SHOWCOLORHINTS
	GIT_PS1_SHOWDIRTYSTATE=1 ; export GIT_PS1_SHOWDIRTYSTATE
	GIT_PS1_SHOWSTASHSTATE=1 ; export GIT_PS1_SHOWSTASHSTATE
	GIT_PS1_SHOWUNTRACKEDFILES=1 ; export GIT_PS1_SHOWUNTRACKEDFILES
	# PS1='\w$(__git_ps1 " (%s)")\$ '
	PS1=`printf "%s" "${PS1}" | sed -e 's/\\\\$ $/$(__git_ps1 " (%s)")\\\\$ /' -`

	PS1=`printf "%s" "${PS1}" | sed -e 's/\\\\$ $/$(__docker_machine_ps1 " [%s]")\\\\$ /' -`
fi

# Python Env Wrapper (pew) and virtualenvwrapper virtualenvs location
# See the following links:
#    * https://github.com/berdario/pew#configuration
#    * https://virtualenvwrapper.readthedocs.io/en/latest/install.html#location-of-environments
#    * https://docs.pipenv.org/advanced.html#custom-virtual-environment-location
#
WORKON_HOME="${XDG_CACHE_HOME:-$HOME/.cache}/virtualenvs" ; export WORKON_HOME

# Packer phone home function
CHECKPOINT_DISABLE=1 ; export CHECKPOINT_DISABLE
PACKER_CONFIG="${HOME}/.config/packer/packerconfig" ; export PACKER_CONFIG

# .NET Core / NuGet package restore location
DOTNET_PACKAGES="${XDG_CACHE_HOME:-$HOME/.cache}/nuget/packages" ; export DOTNET_PACKAGES
# .NET Core / VS Code phone home function
DOTNET_CLI_TELEMETRY_OPTOUT=1 ; export DOTNET_CLI_TELEMETRY_OPTOUT

# Gradle
GRADLE_USER_HOME="${XDG_CACHE_HOME:-$HOME/.cache}/gradle" ; export GRADLE_USER_HOME

# HTTPie
if [ -d "${XDG_CONFIG_HOME:-$HOME/.config}/httpie" ] ; then
    HTTPIE_CONFIG_DIR="${XDG_CONFIG_HOME:-$HOME/.config}/httpie" ; export HTTPIE_CONFIG_DIR
fi

# NodeJS
NODE_REPL_HISTORY="${XDG_CACHE_HOME:-$HOME/.cache}/node/node_repl_history" ; export NODE_REPL_HISTORY

# Node Package Manager (npm) config updates
NPM_CONFIG_CACHE="${XDG_CACHE_HOME:-$HOME/.cache}/npm" ; export NPM_CONFIG_CACHE
NPM_CONFIG_GLOBALCONFIG=/etc/npm/npmrc ; export NPM_CONFIG_GLOBALCONFIG
NPM_CONFIG_GLOBALIGNOREFILE=/etc/npm/npmignore ; export NPM_CONFIG_GLOBALIGNOREFILE

if [ -f "${XDG_CONFIG_HOME:-$HOME/.config}/npm/npmrc" ] ; then
    NPM_CONFIG_USERCONFIG="${XDG_CONFIG_HOME:-$HOME/.config}/npm/npmrc" ; export NPM_CONFIG_USERCONFIG
fi

NPM_CONFIG_PREFIX="${HOME}/.local" ; export NPM_CONFIG_PREFIX

# Gauge
# Following line interferes with man Bash Completion
# MANPATH=":${HOME}/.gauge/man" ; export MANPATH

# tmux
TMUX_TMPDIR="$XDG_RUNTIME_DIR" ; export TMUX_TMPDIR

# See <https://www.vagrantup.com/docs/cli/aliases.html> for more information.
VAGRANT_ALIAS_FILE="${XDG_CONFIG_HOME:-$HOME/.config}/vagrant/aliases"; export VAGRANT_ALIAS_FILE

if [ -n "${BASH_VERSION}" ] ; then
	if [ -f "${HOME}/.bashrc" ] || [ -L "${HOME}/.bashrc" ] ; then
		. "${HOME}/.bashrc"
	fi
else
	if [ -f "${HOME}/.shinit" ] ; then
		ENV="${HOME}/.shinit" ; export ENV
	fi
fi

