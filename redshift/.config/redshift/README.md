# Further redshift.conf changes skipped

The `redshift.conf` file includes the necessary settings so that [redshift](https://packages.debian.org/redshift) and [redshift-gtk](https://packages.debian.org/redshift-gtk) should work *out of the box* as long as [geoclue-2.0](https://packages.debian.org/geoclue-2.0) is installed.

The following command has been applied to the `redshift.conf` file in my local repository in order to prevent any of my further changes to the file being recognised by Git:

```shell
git update-index --skip-worktree redshift/.config/redshift/redshift.conf
```

The following command can be used to reverse things:

```shell
git update-index --no-skip-worktree redshift/.config/redshift/redshift.conf
```

## External Links

For additional information, see the following:

1. [How can I make git ignore future revisions to a file?](https://stackoverflow.com/questions/4348590/how-can-i-make-git-ignore-future-revisions-to-a-file)
2. [Git skip-worktree and how I used to hate config files](https://medium.com/@igloude/git-skip-worktree-and-how-i-used-to-hate-config-files-e84a44a8c859)

